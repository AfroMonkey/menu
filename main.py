import json
from datetime import datetime, time, timedelta

import googlemaps
from pydantic.json import pydantic_encoder

from menulocal.domain.coords import GeoCoords
from menulocal.domain.period import GoogleDate, Period
from menulocal.domain.place import Place
from menulocal.domain.route_optimizer import RouteOptimizer
from menulocal.domain.timer import Timer
from menulocal.infra import envars


def main():
    start_point = (20.733937388096983, -103.31079638237074)  # Home
    start_point = (20.678222, -103.368801)
    start_point = (20.667827960684782, -103.36833732903658)
    PLACE_TYPE = "bar"
    start_time = Timer.new("22:00")
    end_time = Timer.new("23:59")
    day = 1
    RADIUS = 10_000

    gmaps = googlemaps.Client(key=envars.GOOGLE_MAPS_KEY)
    # When
    res = gmaps.places_nearby(
        radius=RADIUS,
        location=start_point,
        type=PLACE_TYPE,
    )
    # Then
    places = res["results"]
    assert len(places) > 0
    details = (
        gmaps.place(place["place_id"])["result"]
        for place in places
        if place["business_status"] == "OPERATIONAL" and place.get("opening_hours")
    )
    places = [
        Place(
            name=d["name"],
            coords=GeoCoords.parse_obj(d["geometry"]["location"]),
            periods=[Period.parse_obj(p) for p in d["opening_hours"]["periods"]],
        )
        for d in details
    ]
    places = {p.name: p for p in places}  # Assume no duplicate names
    schedule = RouteOptimizer(
        time_spent_in_place=timedelta(minutes=30),
        time_spent_in_travel_one_km=timedelta(minutes=10),
        places=places,
        start_coords=GeoCoords(lat=start_point[0], lng=start_point[1]),
        start_time=start_time,
        end_time=end_time,
        day=day,
    ).optimize()
    print(f"Visiting {len(schedule) - 2} places:")
    for s in schedule:
        print(
            f"Place: {s[0].name}, Time traveling to place: {s[1]}, Arrive time: {s[2]}, Leave time: {s[3]}"
        )


if __name__ == "__main__":
    main()
