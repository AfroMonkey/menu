import json
from datetime import datetime

import googlemaps
from menulocal.domain.coords import GeoCoords
from menulocal.domain.period import Period
from menulocal.domain.place import Place
from menulocal.infra import envars
from pydantic.json import pydantic_encoder


def test_get_locations():
    # Given
    start_point = (20.733937388096983, -103.31079638237074)
    PLACE_TYPE = "bar"
    RADIUS = 10_000
    gmaps = googlemaps.Client(key=envars.GOOGLE_MAPS_KEY)
    # When
    res = gmaps.places_nearby(
        radius=RADIUS,
        location=start_point,
        type=PLACE_TYPE,
    )
    # Then
    places = res["results"]
    assert len(places) > 0
    details = (
        gmaps.place(place["place_id"])["result"]
        for place in places
        if place["business_status"] == "OPERATIONAL" and place.get("opening_hours")
    )
    places = [
        Place(
            name=d["name"],
            coords=GeoCoords.parse_obj(d["geometry"]["location"]),
            periods=[Period.parse_obj(p) for p in d["opening_hours"]["periods"]],
        )
        for d in details
    ]
    with open("data.json", "w") as f:
        f.write(json.dumps(places, default=pydantic_encoder))
