import json
from datetime import time, timedelta

from menulocal.domain.coords import GeoCoords
from menulocal.domain.period import GoogleDate
from menulocal.domain.place import Place
from menulocal.domain.route_optimizer import RouteOptimizer
from menulocal.domain.timer import Timer


def test_good_route():
    with open("data.json", "r") as f:
        places_json = json.load(f)
        places = (Place.parse_obj(p) for p in places_json)
        places = {p.name: p for p in places}  # Assume no duplicate names
        schedule = RouteOptimizer(
            time_spent_in_place=timedelta(minutes=30),
            time_spent_in_travel_one_km=timedelta(minutes=10),
            places=places,
            start_coords=GeoCoords(lat=20.733937388096983, lng=-103.31079638237074),
            start_time=Timer.new("17:00"),
            end_time=Timer.new("23:59"),
            day=1,
        ).optimize()
