```python
    res = gmaps.directions(
        origin=start_point,
        destination=start_point,
        waypoints=waypoints,
        mode="walking",
        departure_time=start_time,
        optimize_waypoints=True,
    )
    res = gmaps.distance_matrix(
        origins=[start_point],
        destinations=waypoints,
        mode="walking",
        departure_time=start_time,
    )
```
