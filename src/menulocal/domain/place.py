from datetime import datetime
from typing import List

from menulocal.domain.coords import GeoCoords
from menulocal.domain.period import Period
from pydantic import BaseModel


class Place(BaseModel):
    name: str
    coords: GeoCoords
    periods: List[Period]

    def open_in_time_window(
        self, day: int, arrival_datetime: datetime, leave_datetime: datetime
    ) -> bool:
        valid_periods = [p for p in self.periods if p.open.day == day]
        pass
        return any(
            p.close.day > day
            or (
                p.open.to_datetime() <= arrival_datetime <= p.close.to_datetime()
                and p.open.to_datetime() <= leave_datetime <= p.close.to_datetime()
            )
            for p in valid_periods
        )
