from datetime import datetime

from pydantic import BaseModel


class GoogleDate(BaseModel):
    day: int
    time: str

    # Assumptions: No change in week for comparison

    def __eq__(self, other):
        return self.day == other.day and self.time == other.time

    def __lt__(self, other):
        return self.day < other.day or (self.day == other.day and self.time < other.time)

    def __gt__(self, other):
        return self.day > other.day or (self.day == other.day and self.time > other.time)

    def to_datetime(self) -> datetime:
        hour, minute = self.time[:2], self.time[3:]
        date = datetime.fromisoformat("2020-01-01")
        date = date.replace(hour=int(hour), minute=int(minute))
        return date


class Period(BaseModel):
    open: GoogleDate
    close: GoogleDate
