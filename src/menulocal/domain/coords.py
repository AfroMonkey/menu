from math import cos, sin
from typing import Tuple

from haversine import haversine
from pydantic import BaseModel


# @dataclass
class GeoCoords(BaseModel):
    lat: float
    lng: float

    def to_tuple(self) -> Tuple[float, float]:
        return self.lat, self.lng

    def distance_to_point(self, point: "GeoCoords") -> float:
        return haversine(self.to_tuple(), point.to_tuple())
