from datetime import datetime


class Timer:
    @staticmethod
    def new(time_str: str) -> datetime:
        hour, minute = time_str.split(":")
        date = datetime.fromisoformat("2020-01-01")
        date = date.replace(hour=int(hour), minute=int(minute))
        return date
