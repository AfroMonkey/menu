import logging
from datetime import datetime, time, timedelta
from typing import Dict, List, Set, Tuple

from menulocal.domain.coords import GeoCoords
from menulocal.domain.period import GoogleDate
from menulocal.domain.place import Place
from pydantic import BaseModel

Schedule = List[Tuple[Place, timedelta, datetime, datetime]]
_logger = logging.getLogger(__name__)


class RouteOptimizer(BaseModel):
    time_spent_in_place: timedelta
    time_spent_in_travel_one_km: timedelta
    places: Dict[str, Place]
    start_coords: GeoCoords
    day: int
    start_time: datetime
    end_time: datetime

    def optimize(self) -> Schedule:
        current_place = self.start_coords
        home = Place(name="Home", coords=self.start_coords, periods=[])
        current_date = self.start_time
        schedule: Schedule = [(home, timedelta(), current_date, current_date)]
        last_time_to_home = current_date
        while "Has time left":
            places_ranked_by_distance = sorted(
                self.places.values(), key=lambda p: p.coords.distance_to_point(current_place)
            )

            for place in places_ranked_by_distance:
                (
                    arrival_time,
                    leave_time,
                    time_to_home,
                ) = self.times_per_place(current_place, place, current_date)
                if time_to_home <= self.end_time and place.open_in_time_window(
                    self.day, arrival_time, leave_time
                ):
                    schedule.append(
                        (
                            self.places.pop(place.name),
                            arrival_time - current_date,
                            arrival_time,
                            leave_time,
                        )
                    )
                    current_place = place.coords
                    current_date = leave_time
                    last_time_to_home = time_to_home
                    break
                else:
                    _logger.debug(f"Place {place.name} not reachable in time")
            else:
                _logger.debug("No more places can be visited")
                break
        schedule.append(
            (home, last_time_to_home - current_date, last_time_to_home, last_time_to_home)
        )
        return schedule

    def times_per_place(
        self, origin: GeoCoords, destination_place: Place, current_date: datetime
    ) -> Tuple[datetime, datetime, datetime]:
        destination = destination_place.coords
        time_to_arrive = timedelta(minutes=self.get_time_to_arrive_in_minutes(origin, destination))
        arrival_datetime = current_date + time_to_arrive
        leave_datetime = arrival_datetime + self.time_spent_in_place
        timedelta_to_home = timedelta(
            minutes=self.get_time_to_arrive_in_minutes(destination, self.start_coords)
        )
        roundtrip_datetime = leave_datetime + timedelta_to_home

        return arrival_datetime, leave_datetime, roundtrip_datetime

    def get_time_to_arrive_in_minutes(
        self, origin_place: GeoCoords, destination_place: GeoCoords
    ) -> float:
        distance_in_km = origin_place.distance_to_point(destination_place)
        return (self.time_spent_in_travel_one_km.seconds / 60) * distance_in_km
